FROM alpine:latest AS builder

ADD *.c *.h /incapache/
WORKDIR /incapache
RUN apk update && apk add gcc musl-dev libc6-compat
RUN gcc -DLOG_USE_COLOR -DDEBUG *.c -o incapache -lpthread

FROM alpine:latest
RUN apk update && apk add file curl python3 py-pip
RUN  adduser -s /bin/bash web -D
WORKDIR /home/web/incapache
ADD www www
COPY --from=builder incapache .
RUN chown root incapache && chmod u+s incapache
USER web
CMD [ "./incapache", "www","8888" ] 
