let theme = window.localStorage.getItem("theme");

if (theme) {
	theme === "dark" ? document.body.classList.add("dark-theme") : (document.body.className = "");
}
else {
	if (window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches) {
		document.body.classList.add("dark-theme");
	}
	else {
		document.body.className = "";
	}
}

function change_theme() {
	document.body.classList.toggle("dark-theme");
	window.localStorage.setItem(
		"theme",
		document.body.classList.contains("dark-theme") ? "dark" : "light"
	);
}

coin = document.getElementById("coin");
coin.onclick = function () {
	var flipResult = Math.random();
	coin.className = "";
	setTimeout(function () {
		if (flipResult <= 0.5) {
			coin.classList.add("heads");
		} else {
			coin.classList.add("tails");
		}
	}, 100);
};
