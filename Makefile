CFLAGS= -DLOG_USE_COLOR #-DDEBUG 
				
OBJS=aux.o http.o main.o threads.o
EXE=incapache

$(EXE): $(OBJS) 
	$(CC) $(CFLAGS) log.c  $(OBJS) -o $(EXE)  -lpthread
	sudo chown root $(EXE)
	sudo chmod u+s $(EXE)

aux.o: aux.c incapache.h
http.o: http.c incapache.h
main.o: main.c incapache.h
threads.o: threads.c incapache.h

clean:
	rm -f $(OBJS) $(EXE)