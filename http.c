/* 
 * incApache_aux.c: funzioni ausiliarie per il web server
 *
 * Programma sviluppato a supporto del laboratorio di
 * Sistemi di Elaborazione e Trasmissione del corso di laurea
 * in Informatica classe L-31 presso l'Universita` degli Studi di
 * Genova per l'anno accademico 2020/2021.
 *
 * Copyright (C) 2012-2014 by Giovanni Chiola <chiolag@acm.org>
 * Copyright (C) 2015-2016 by Giovanni Lagorio <giovanni.lagorio@unige.it>
 * Copyright (C) 2016-2020 by Giovanni Chiola <chiolag@acm.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include "incapache.h"

ssize_t send_all(int fd, const char *ptr, size_t n, int flags){
	size_t n_left = n;
	while (n_left > 0){
		ssize_t n_written = send(fd, ptr, n_left, flags);
		if (n_written < 0){
			if (n_left == n)
				return -1; /* nothing has been sent */
			else
				break; /* we have sent something */
		}
		else if (n_written == 0)
			break;

		n_left -= n_written;
		ptr += n_written;
	}
	assert(n - n_left >= 0);
	return n - n_left;
}

static int CurUID = 0;
static int UserTracker[MAX_COOKIES];
pthread_mutex_t cookie_mutex = PTHREAD_MUTEX_INITIALIZER;
int get_new_UID(void){
	int retval;
	// Compute retval by incrementing CurUID mod MAX_COOKIES and reset UserTracker[retval] to 0. Be careful in order to avoid race conditions
	if (pthread_mutex_lock(&cookie_mutex) != 0)
		fail_errno("incapache: error while locking cookie_mutex");
	retval = (CurUID++) % MAX_COOKIES;
	UserTracker[retval] = 0;
	if (pthread_mutex_unlock(&cookie_mutex) != 0)
		fail_errno("incapache: error while unlocking cookie_mutex");
	return retval;
}

int keep_track_of_UID(int myUID){
	int newcount;
	if ((myUID < 0) || (myUID >= MAX_COOKIES))
		return -1;
	// Increment UserTracker[myUID] and return the computed value. Be careful in order to avoid race conditions
	if (pthread_mutex_lock(&cookie_mutex) != 0)
		fail_errno("incapache: error while locking cookie_mutex");

	newcount = ++UserTracker[myUID];
	if (pthread_mutex_unlock(&cookie_mutex) != 0)
		fail_errno("incapache: error while unlocking cookie_mutex");
	return newcount;
}

void send_response(int client_fd, int response_code, int cookie, int is_http1_0, int thread_no, char *filename, struct stat *stat_p){
	time_t now_t = time(NULL);
	struct tm now_tm;
	time_t file_modification_time;
	struct tm file_modification_tm;
	char time_as_string[MAX_TIME_STR];
	char http_header[MAX_HEADER_SIZE] = "HTTP/1.";
	size_t header_size;
	int fd = -1;
	off_t file_size = 0;
	char *mime_type = NULL;
	const char *const HTML_mime = "text/html";
	struct stat stat_buffer;
	size_t header_sent;
	log_debug("start send_response(response_code=%d, filename=%s)", response_code, filename);
	// Compute date of servicing current HTTP Request using a variant of gmtime()
	if (gmtime_r(&now_t, &now_tm) == NULL)
		fail_errno("incapache: gtmtime_r error");

	strftime(time_as_string, MAX_TIME_STR, "%a, %d %b %Y %T GMT", &now_tm);
	strcat(http_header, is_http1_0 ? "0 " : "1 ");
	switch (response_code){
		case RESPONSE_CODE_OK:
			strcat(http_header, "200 OK");
			if (filename != NULL){
				if (stat_p != NULL){
					fd = open(filename, O_RDONLY);
					if (fd < 0)
						fail("send_response: cannot open file for reading (has the file vanished?)");
					log_debug("send_response(%d, %s): opened file %d", response_code, filename, fd);
				}
				mime_type = get_mime_type(filename);
				log_debug("send_response(%d, %s): got mime type %s", response_code, filename, mime_type);
				if (stat_p == NULL){
					stat_p = &stat_buffer;
					if (stat(filename, stat_p))
						fail_errno("stat");
				}

				// compute file_size and file_modification_time
				file_size = stat_p->st_size;
				file_modification_time = stat_p->st_mtime;
				log_debug("send_response(%3d,%s) : file opened, size=%lu, mime=%s", response_code, filename, (unsigned long)file_size, mime_type);
			}
			break;
		case RESPONSE_CODE_NOT_MODIFIED:
			strcat(http_header, "304 Not Modified");
			break;
		case RESPONSE_CODE_BAD_REQUEST:
			strcat(http_header, "400 Bad Request");
			break;
		case RESPONSE_CODE_NOT_FOUND:
			strcat(http_header, "404 Not Found");
			if ((fd = open(HTML_404, O_RDONLY)) >= 0){
				// compute file_size, mime_type, and file_modification_time of HTML_404
				mime_type = get_mime_type(HTML_404);
				log_debug("send_response(%d, %s): got mime type %s", response_code, filename, mime_type);
				if (stat_p == NULL){
					stat_p = &stat_buffer;
					if (stat(HTML_404, stat_p))
						fail_errno("incapache: error in stat 404");
				}

				file_size = stat_p->st_size;
				file_modification_time = stat_p->st_mtime;
			}
			break;
		default:
			strcat(http_header, "501 Method Not Implemented\r\nAllow: HEAD,GET");
			if ((fd = open(HTML_501, O_RDONLY)) >= 0){
				// compute file_size, mime_type, and file_modification_time of HTML_501
				mime_type = get_mime_type(HTML_501);
				log_debug("send_response(%d, %s): got mime type %s", response_code, filename, mime_type);
				if (stat_p == NULL){
					stat_p = &stat_buffer;
					if (stat(HTML_501, stat_p))
						fail_errno("incapache: error in stat 501");
				}
				file_size = stat_p->st_size;
				file_modification_time = stat_p->st_mtime;
			}
			break;
		}
	strcat(http_header, "\r\nDate: ");
	strcat(http_header, time_as_string);
	if (cookie >= 0){
		// set permanent cookie in order to identify this client
		now_tm.tm_year++;
		strftime(time_as_string, MAX_TIME_STR, "%a, %d %b %Y %T GMT", &now_tm);
		sprintf(http_header + strlen(http_header), "\r\nSet-Cookie: id=%d; Expires=%s;", cookie, time_as_string);
	}
	strcat(http_header, "\r\nServer: incApache for SETI.\r\n");
	if (response_code == 501 || is_http1_0)
		strcat(http_header, "Connection: close\r\n");
	if (file_size > 0 && mime_type != NULL){
		sprintf(http_header + strlen(http_header), "Content-Length: %lu \r\nContent-Type: %s\r\nLast-Modified: ", (unsigned long)file_size, mime_type);
		// compute time_as_string, corresponding to file_modification_time, in GMT standard format; see gmtime and strftime
		if (gmtime_r(&file_modification_time, &file_modification_tm) == NULL)
			fail_errno("incapache: error in gmtime_r");
		strftime(time_as_string, MAX_TIME_STR, "%a, %d %b %Y %T GMT", &file_modification_tm);

		strcat(http_header, time_as_string);
		strcat(http_header, "\r\n");
	}
	strcat(http_header, "\r\n");
	log_debug("send_response(%d, %s) : header prepared", response_code, filename);
	log_debug("Sending the following response:\n\n\n%s", http_header);
	header_size = strlen(http_header);
	join_prev_thread(thread_no);
	if ((header_sent = send_all(client_fd, http_header, header_size, (fd >= 0 ? MSG_MORE : 0))) < header_size){
		if (header_sent == -1)
			fail_errno("incApache: could not send HTTP header");
		debug("header partially sent; header_size=%lu, header_sent=%lu\n", (unsigned long)header_size, (unsigned long)header_sent);
		close(fd);
		fd = -1;
	}
	log_debug("send_response(%d, %s): header sent", response_code, filename);
	if (fd >= 0){
		// send fd file on client_fd, then close fd; see syscall sendfile 
		if (sendfile(client_fd, fd, 0, file_size) == -1)
			fail_errno("incapache: error in sendfile");
		if (close(fd) == -1)
			fail_errno("incapache: error in close fd");
	}
	if (fd >= 0){
		int optval = 1;
		if (setsockopt(client_fd, IPPROTO_TCP, TCP_NODELAY, &optval, sizeof(int)))
			fail_errno("Cannot set socket options");
	}
	log_debug("end send_response(%d, %s)", response_code, filename);
	free(mime_type);
}

void manage_http_requests(int client_fd, int connection_no){
	#define METHOD_NONE 0
	#define METHOD_HEAD 1
	#define METHOD_GET 2
	#define METHOD_NOT_CHANGED 4
	#define METHOD_CONDITIONAL 8
	#define MethodIsConditional(m) ((m)&METHOD_CONDITIONAL)
	FILE *client_stream = fdopen(client_fd, "r");
	char *http_request_line = NULL;
	char *strtokr_save;
	size_t n = 0;
	int http_method;
	struct tm since_tm;
	struct stat *stat_p;
	int UIDcookie = -1;
	int is_http1_0 = 0;
	int thread_idx;
	if (!client_stream)
		fail_errno("cannot open client stream");
	while (getline(&http_request_line, &n, client_stream) >= 0){
		char *method_str, *filename, *protocol;
		char *http_option_line = NULL;
		char *option_name, *option_val;
		http_request_line[strcspn(http_request_line, "\n")] = 0;

		log_debug("Received the following request: %s", http_request_line);
		thread_idx = find_unused_thread_idx(connection_no);

		// parse first line defining the 3 strings method_str, filename, and protocol
		method_str = strtok(http_request_line, " ");
		filename = strtok(NULL, " ");
		protocol = strtok(NULL, "\r");

		log_debug("method_str=%s, filename=%s (0=%c), protocol=%s (len=%d)",
				  method_str, filename, filename ? filename[0] : '?', protocol, (int)(protocol ? strlen(protocol) : 0));
		if (method_str == NULL || filename == NULL || protocol == NULL ||
			filename[0] != '/' || strncmp(protocol, "HTTP/1.", 7) != 0 ||
			strlen(protocol) != 8){
			log_debug("bad Request!\n");
			SEND_RESPONSE(client_fd, RESPONSE_CODE_BAD_REQUEST, UIDcookie, 1, connection_no, thread_idx, NULL, NULL);
			free(http_request_line);
			break;
		}
		is_http1_0 = !strcmp(protocol, "HTTP/1.0");
		memset(&since_tm, 0, sizeof(since_tm));
		http_method = METHOD_NONE;
		if (strcmp(method_str, "GET") == 0)
			http_method = METHOD_GET;
		else if (strcmp(method_str, "HEAD") == 0)
			http_method = METHOD_HEAD;
		log_debug("http_method=%d\n\n", http_method);
		for (http_option_line = NULL, n = 0;
			 getline(&http_option_line, &n, client_stream) >= 0 && strcmp(http_option_line, "\r\n") != 0;
			 free(http_option_line), http_option_line = NULL, n = 0){
			debug("http_option_line: %s", http_option_line);
			option_name = strtok_r(http_option_line, ": ", &strtokr_save);
			if (option_name != NULL){
				if (strcmp(option_name, "Cookie") == 0){
					// parse the cookie in order to get the UserID and count the number of requests coming from this client
					strtok_r(NULL, "=", &strtokr_save);
					UIDcookie = atoi(strtok_r(NULL, " ", &strtokr_save));
				}
				if (!MethodIsConditional(http_method)){
					// parse option line, recognize "If-Modified-Since" option, and possibly add METHOD_CONDITIONAL flag to http_method
					if (strcmp(option_name, "If-Modified-Since") == 0)
					{
						// strftime(strtokr_save, MAX_TIME_STR, "%a, %d %b %Y %T GMT", &since_tm);
						strptime(strtokr_save, "%a, %d %b %Y %T GMT", &since_tm);
						http_method = METHOD_CONDITIONAL;
					}
				}
			}
		}
		free(http_option_line);
		// increment visit count for this user
		if (UIDcookie >= 0){ 
			int current_visit_count = keep_track_of_UID(UIDcookie);
			// wrong Cookie value
			if (current_visit_count < 0)
				UIDcookie = get_new_UID();
			else{
				log_info("client provided UID Cookie %d for the %d time", UIDcookie, current_visit_count);
				UIDcookie = -1;
			}
		}
		/*** user did not provide any Cookie ***/
		else 
			UIDcookie = get_new_UID();

		if (http_method == METHOD_NONE){
			log_info("method not implemented\n");
			SEND_RESPONSE(client_fd, 501, UIDcookie, 1, connection_no, thread_idx, method_str, NULL);
			break;
		}
		if (strcmp(filename, "/") == 0)
			filename = "index.html";
		else
			filename += 1; /* remove leading '/' */
		debug("\n\n");
		log_debug("http_method=%d, filename=%s", http_method, filename);
		stat_p = my_malloc(sizeof(*stat_p));
		if (access(filename, R_OK) != 0 || stat(filename, stat_p) < 0){
			log_debug("file %s not found!", filename);
			free(stat_p);
			SEND_RESPONSE(client_fd, RESPONSE_CODE_NOT_FOUND, UIDcookie, is_http1_0, connection_no, thread_idx, filename, NULL);
		}
		else{
			if (MethodIsConditional(http_method)){
				// compare file last modification time and decide whether to transform http_method to METHOD_NOT_CHANGED
				if ((int)my_timegm(&since_tm) > (int)stat_p->st_mtime)
					http_method = METHOD_NOT_CHANGED;
			}
			switch (http_method){
			case METHOD_HEAD:
				log_debug("sending header for file %s", filename);
				free(stat_p);
				SEND_RESPONSE(client_fd, RESPONSE_CODE_OK, UIDcookie, /*** OK, without body ***/ is_http1_0, connection_no, thread_idx, filename, NULL);
				break;
			case METHOD_NOT_CHANGED:
				log_debug("file %s not modified", filename);
				free(stat_p);
				SEND_RESPONSE(client_fd, RESPONSE_CODE_NOT_MODIFIED, UIDcookie, /*** Not Modified, without body ***/ is_http1_0, connection_no, thread_idx, NULL, NULL);
				break;
			case METHOD_GET:
				log_debug("sending file %s", filename);
				SEND_RESPONSE(client_fd, RESPONSE_CODE_OK, UIDcookie, /*** OK, with body ***/ is_http1_0, connection_no, thread_idx, filename, stat_p);
				break;
			default:
				assert(0);
			}
		}
		if (is_http1_0)
			break;
	}
	join_all_threads(connection_no);
	if (close(client_fd))
		fail_errno("Cannot close the connection");
}
